import argparse
import os
import platform
import shutil
import time
from pathlib import Path
import numpy as np
import cv2
from math import sqrt
import torch
import torch.backends.cudnn as cudnn
from numpy import random
import scipy.stats as stats
import matplotlib.pyplot as plt
from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import (
    check_img_size, non_max_suppression, apply_classifier, scale_coords, xyxy2xywh, plot_one_box, strip_optimizer,xywh2xyxy)
from utils.torch_utils import select_device, load_classifier, time_synchronized


def detect(save_img=False):
    out, source, weights, view_img, save_txt, imgsz = \
        opt.output, opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size
    webcam = source == '0' or source.startswith('rtsp') or source.startswith('http') or source.endswith('.txt')

    # Initialize
    device = select_device(opt.device)
    if os.path.exists(out):
        shutil.rmtree(out)  # delete output folder
    os.makedirs(out)  # make new output folder
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    imgsz = check_img_size(imgsz, s=model.stride.max())  # check img_size
    if half:
        model.half()  # to FP16

    # Second-stage classifier
    classify = False
    if classify:
        modelc = load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt', map_location=device)['model'])  # load weights
        modelc.to(device).eval()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = True
        cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz)
    else:
        save_img = True
        dataset = LoadImages(source, img_size=imgsz)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]

    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
    _ = model(img.half() if half else img) if device.type != 'cpu' else None  # run once
    ok=False
    farm=0
    tlist=list()
    boxes=list()
    status=list()
    oldboxes=list()
    velist=list()
    count=0
    for path, img, im0s, vid_cap in dataset:
        count=count+1
        if count%10==0:
            status.append(False)
        print(len(tlist))
        print(len(boxes))
        print(len(status))
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)
        new=im0s
        new = cv2.cvtColor(new,cv2.COLOR_BGR2GRAY)
        if count==1:
            prev=new
        height=len(im0s[1])
        halfstrip=int(0.01*height)
        sec_per_frame=0.033306 #~30FPS
        carwidth=1.8
        # Inference
        t1 = time_synchronized()
        #begin chutiayapa
        if all(status) and status:
            flow = cv2.calcOpticalFlowFarneback(prev,new, None, 0.5, 3, 15, 3, 5, 1.2, 0)
            mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
            boxes.clear()
            status.clear()
            for tracker in tlist:
                ok, bbox = tracker.update(im0s)
                if bbox[2]<5 or bbox[3]<5:
                    ok=False
                    tlist.remove(tracker)
                if ok:
                    boxes.append(bbox)
                    status.append(ok)
                    al=mag[int(bbox[1])-halfstrip:int(bbox[1])+halfstrip ,:]
                    newbox=bbox
                    if newbox[2]<newbox[3]:
                        w=newbox[3]
                    else:
                        w=newbox[2]
                    index=tlist.index(tracker)
                    oldbox=oldboxes[index]
                    x=newbox[0]-oldbox[0]
                    y=newbox[1]-oldbox[1]
                    boxvel=sqrt((x*x)+(y*y))
                    #hmean=0
                    farm=True
                    if farm:
                        h=al
                        h.sort()
                        hmean = np.mean(h)
                        #hstd = np.std(h)
                        #pdf = stats.norm.pdf(h, hmean, hstd) 
                        #plt.plot(h, pdf)
                        #plt.savefig("mag.png")
                        farm=True
                    pixel_vel=abs(hmean-boxvel)/sec_per_frame
                    real_vel= (carwidth/w)*pixel_vel*3.6
                    if real_vel>200:
                        real_vel=83.0 #hack,need to fix
                    velist.append(real_vel)
                    oldboxes.remove(oldbox)
                    oldboxes.insert(index,newbox)   
                    print("Speed:")
                    print(real_vel)                 
        else:
            tlist.clear()
            boxes.clear()
            status.clear()
            pred = model(img, augment=opt.augment)[0]

        # Apply NMS
            pred = non_max_suppression(pred, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        
        
        t2 = time_synchronized()
        prev=new
        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)

        if all(status) and status:
                    # Tracking success
            # p1 = (int(bbox[0]), int(bbox[1]))
            # p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            # cv2.rectangle(im0s, p1, p2, (255,0,0), 2, 1)
            for bbox in boxes:
                ind=boxes.index(bbox)
                bbox=np.array(bbox)
                xyxy=(xywh2xyxy(torch.tensor(bbox).view(1, 4))).view(-1).tolist()
                if int(velist[ind])<100:
                    plot_one_box(xyxy, im0s, label="Vehicle:"+str(int(velist[ind]))+"km/hr", color=[0,255,0], line_thickness=3)
                else:
                    plot_one_box(xyxy, im0s, label="Vehicle:"+str(int(velist[ind]))+"km/hr", color=[0,0,255], line_thickness=3)
            velist.clear()

            if save_img:
                if dataset.mode == 'images':
                    cv2.imwrite(save_path, im0)
                else:
                    if vid_path != save_path:  # new video
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            vid_writer.release()  # release previous video writer

                        fourcc = 'mp4v'  # output video codec
                        fps = vid_cap.get(cv2.CAP_PROP_FPS)
                        w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                        h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        vid_writer = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*fourcc), fps, (w, h))
                    fps_f=(1/(t2 - t1))
                    cv2.putText(im0s, "Tracking at "+str(int(fps_f))+" FPS", (100,80), cv2.FONT_HERSHEY_SIMPLEX, 2,(0,255,0),3)
                    vid_writer.write(im0s)
            
            print('Done. FPS:(%.3f)' % fps_f)
            

        else :
                # Tracking failure
            print("Initializing")
            cv2.putText(im0s, "Initialized", (100,80), cv2.FONT_HERSHEY_SIMPLEX, 2,(255,0,0),3)        
             # Process detections
            for i, det in enumerate(pred):  # detections per image
                if webcam:  # batch_size >= 1
                    p, s, im0 = path[i], '%g: ' % i, im0s[i].copy()
                else:
                    p, s, im0 = path, '', im0s

                save_path = str(Path(out) / Path(p).name)
                txt_path = str(Path(out) / Path(p).stem) + ('_%g' % dataset.frame if dataset.mode == 'video' else '')
                s += '%gx%g ' % img.shape[2:]  # print string
                gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
                if det is not None and len(det):
                    # Rescale boxes from img_size to im0 size
                    det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                    # Print results
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class
                        s += '%g %ss, ' % (n, names[int(c)])  # add to string

                    # Write results
                    l=0
                    for *xyxy, conf, cls in det:
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4))).view(-1).tolist()
                        print(xywh)
                        xywh=np.array(xywh)
                        bbox=tuple([int(xywh[0]),int(xywh[1]),int(xywh[2]),int(xywh[3])])
                        if bbox[2]>1 and bbox[3]>1 and conf>0.1:
                            tracker = cv2.TrackerMOSSE_create()
                            ok = tracker.init(im0, bbox)
                            tlist.append(tracker)
                            boxes.append(bbox)
                            status.append(ok)
                            oldboxes.append(bbox)
                        if save_txt:  # Write to file
                            #xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                            with open(txt_path + '.txt', 'a') as f:
                                f.write(('%g ' * 5 + '\n') % (cls, *xywh))  # label format

                        if save_img or view_img:  # Add bbox to image
                            label = '%s %.2f' % (names[int(cls)], conf)
                            plot_one_box(xyxy, im0, label=label, color=[0,255,0], line_thickness=3)

                # Print time (inference + NMS)
                print('%sDone. (%.3fs)' % (s, t2 - t1))

                # Stream results
                if view_img:
                    cv2.imshow(p, im0)
                    if cv2.waitKey(1) == ord('q'):  # q to quit
                        raise StopIteration

                # Save results (image with detections)
                if save_img:
                    if dataset.mode == 'images':
                        cv2.imwrite(save_path, im0)
                    else:
                        if vid_path != save_path:  # new video
                            vid_path = save_path
                            if isinstance(vid_writer, cv2.VideoWriter):
                                vid_writer.release()  # release previous video writer

                            fourcc = 'mp4v'  # output video codec
                            fps = vid_cap.get(cv2.CAP_PROP_FPS)
                            w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                            h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                            vid_writer = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*fourcc), fps, (w, h))
                        vid_writer.write(im0)

    if save_txt or save_img:
        print('Results saved to %s' % Path(out))
        if platform.system() == 'Darwin' and not opt.update:  # MacOS
            os.system('open ' + save_path)

    print('Done. (%.3fs)' % (time.time() - t0))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default='yolov5s.pt', help='model.pt path(s)')
    parser.add_argument('--source', type=str, default='inference/images', help='source')  # file/folder, 0 for webcam
    parser.add_argument('--output', type=str, default='inference/output', help='output folder')  # output folder
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.4, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.5, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    opt = parser.parse_args()
    print(opt)

    with torch.no_grad():
        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['yolov5s.pt', 'yolov5m.pt', 'yolov5l.pt', 'yolov5x.pt']:
                detect()
                strip_optimizer(opt.weights)
        else:
            detect()
